/*
--------------------------------------------------------------------------
Description :

This is the source code of a project to creat smart blindness can
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Used Materiel : 
		- HC-SR04
		- Arduino Uno
		- Buzzer
--------------------------------------------------------------------------
*/

#include <Ultrasonic.h>

Ultrasonic ultrasonic(5,6); // (Trig PIN,Echo PIN)

const int buzzer = 2;
const int DANGEROUSDISTANCE = 20; // The dangerous distance for the person
const int MAXDISTANCE = 50; // Max distance for the hc-sr04
const int bipTime = 2000;

void setup() {
  pinMode(buzzer,OUTPUT);
  Serial.begin(9600); 
}

void loop()
{ 
  int distance = ultrasonic.Ranging(CM);// Calculating the distance in CM
  
  //  Print the distance in serial monitor
  Serial.print(distance); 
  Serial.println(" cm" );
  //-------------------------------------

  if(distance <= DANGEROUSDISTANCE) digitalWrite(buzzer,HIGH); 
  else if (distance <= 30) bipBip(buzzer,bipTime/3);
      else if(distance <= 40) bipBip(buzzer,bipTime/2);
          else if(distance <= 50) bipBip(buzzer,bipTime);
}

void bipBip(int buz,int t){
   digitalWrite(buz,HIGH);
   delay(t);
   digitalWrite(buz,LOW);
   delay(t);
}
